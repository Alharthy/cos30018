package algorithm;

public class CarSlot {
	public int id;
	public String carname; 
	public String cartype;
	public int priority; 
	public float chargingduration; 
	public float startRequested;
	public float finishRequired;
	public float actualstartTime; 
	
	
	public CarSlot Clone() {
		CarSlot car = new CarSlot();
		car.id = id;
		car.cartype = cartype;
		car.carname = carname;
		car.priority = priority;
		car.chargingduration = chargingduration;
		car.startRequested = startRequested;
		car.actualstartTime = actualstartTime;
		car.finishRequired = finishRequired;
		return car;
	}
}
